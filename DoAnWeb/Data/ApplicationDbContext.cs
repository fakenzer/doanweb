﻿using DoAnWeb.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DoAnWeb.Data;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    // Bảng Article
    public DbSet<Article> articles { get; set; }

    // Bảng Category
    public DbSet<Category> categories { get; set; }



    // Bảng Contract
    public DbSet<Contract> contracts { get; set; }

    // Bảng Customer
    public DbSet<Customer> customers { get; set; }

    // Bảng Employee
    public DbSet<Employee> employees { get; set; }

    // Bảng FAQ
    public DbSet<FAQ> FAQs { get; set; }

    // Bảng FeedBack
    public DbSet<FeedBack> feedBacks { get; set; }

    // Bảng LegalDocument
    public DbSet<LegalDocument> legalDocuments { get; set; }

    // Bảng New
    public DbSet<New> news { get; set; }

    // Bảng Position
    public DbSet<Position> positions { get; set; }

    // Bảng ratingLawyer
    public DbSet<RatingLawyer> ratingLawyers { get; set; }

    // Bảng Service
    public DbSet<Service> services { get; set; }


    // Bảng Task
    public DbSet<theTasks> tasks { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<Reply> Replies { get; set; }

}
