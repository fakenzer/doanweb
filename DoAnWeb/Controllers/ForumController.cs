﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Security.Claims;
using DoAnWeb.Models;
using DoAnWeb.Data;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.IO; // Thêm thư viện này để sử dụng Path và Directory
using Microsoft.AspNetCore.Http; // Thêm thư viện này để sử dụng IFormFile

public class ForumController : Controller
{
    private readonly ApplicationDbContext _context;
    private readonly UserManager<ApplicationUser> _userManager;

    public ForumController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
    {
        _context = context;
        _userManager = userManager;
    }

    public async Task<IActionResult> Index(string keyword)
    {
        // Lấy danh sách bài viết
        var posts = await _context.Posts.Include(p => p.employees).ToListAsync();

        // Nếu có từ khóa tìm kiếm, lọc danh sách bài viết
        if (!string.IsNullOrEmpty(keyword))
        {
            posts = posts.Where(p => p.Title.Contains(keyword) || p.Content.Contains(keyword) || p.employees.FullName.Contains(keyword)).ToList();
        }

        return View(posts);
    }

    public async Task<IActionResult> DetailPost(int id)
    {
        var post = await _context.Posts.Include(p => p.employees).Include(p => p.Comments).FirstOrDefaultAsync(p => p.PostId == id);

        // Lấy tất cả các phản hồi từ cơ sở dữ liệu
        var replies = await _context.Replies.ToListAsync();

        // Truyền danh sách các phản hồi vào view thông qua ViewBag hoặc ViewModel
        ViewBag.Replies = replies;

        return View(post);
    }

    [HttpGet]
    public IActionResult CreatePost()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> CreatePost(Post post, IFormFile image)
    {
        // Lấy thông tin của người dùng đang đăng nhập
        var currentUser = await _userManager.GetUserAsync(User);

        // Liên kết người dùng với bài viết được tạo
        post.employeeId = currentUser.employeeId;

        // Kiểm tra xem người dùng đã chọn tập tin hình ảnh hay chưa
        if (image != null && image.Length > 0)
        {
            // Lấy tên tập tin hình ảnh và tạo đường dẫn lưu trữ trong thư mục wwwroot/images
            var fileName = Path.GetFileName(image.FileName);
            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Images", fileName);

            // Lưu tập tin hình ảnh vào thư mục
            using (var stream = new FileStream(imagePath, FileMode.Create))
            {
                await image.CopyToAsync(stream);
            }

            // Lưu đường dẫn của tập tin hình ảnh vào thuộc tính ImageUrl của bài viết
            post.ImageUrl = "/Images/" + fileName;
        }

        // Tiếp tục lưu bài viết vào cơ sở dữ liệu
        post.DateCreated = DateTime.Now;
        _context.Posts.Add(post);
        await _context.SaveChangesAsync();

        // Trả về trang index hoặc trang chi tiết bài viết vừa được tạo
        return RedirectToAction("Index");
    }


    [HttpPost]
    public async Task<IActionResult> AddComment(Comment comment)
    {
        // Kiểm tra xem người dùng đã đăng nhập chưa
        var currentUser = await _userManager.GetUserAsync(User);
        if (currentUser == null)
        {
            // Nếu người dùng chưa đăng nhập, chuyển hướng đến trang đăng nhập
            return RedirectToAction("Login", "Account");
        }

        // Kiểm tra xem có người dùng tương ứng với employeeId hay không
        var employee = await _context.employees.FindAsync(currentUser.employeeId);
        if (employee == null)
        {
            // Xử lý khi không tìm thấy thông tin employee, có thể chuyển hướng đến trang chính hoặc trang lỗi
            return RedirectToAction("Index", "Forum");
        }

        // Thiết lập thông tin người tạo comment
        comment.Author = employee.FullName;
        comment.DateCreated = DateTime.Now;

        // Lưu comment vào cơ sở dữ liệu
        _context.Comments.Add(comment);
        await _context.SaveChangesAsync();

        // Chuyển hướng đến trang chi tiết bài viết mà comment được thêm vào
        return RedirectToAction("DetailPost", new { id = comment.PostId });
    }
    [HttpPost]
    public async Task<IActionResult> ReplyComment(int parentId, string replyContent)
    {
        var currentUser = await _userManager.GetUserAsync(User);

        if (currentUser == null)
        {
            // Xử lý khi không có người dùng đăng nhập
            return RedirectToAction("Login", "Account"); // Ví dụ: chuyển hướng đến trang đăng nhập
        }

        // Lấy thông tin về Employee của người dùng đang đăng nhập
        var employee = await _context.employees.FindAsync(currentUser.employeeId);

        // Tạo đối tượng Reply mới
        var reply = new Reply
        {
            Content = replyContent,
            DateCreated = DateTime.Now,
            CommentId = parentId, // parentId là CommentId của bình luận mà người dùng đang phản hồi
            Author = employee.FullName // Lưu FullName của người dùng hiện tại vào Author của Reply
        };

        // Lưu phản hồi vào cơ sở dữ liệu
        _context.Replies.Add(reply);
        await _context.SaveChangesAsync();

        // Trả về trang chi tiết bài viết mà bình luận được thêm vào
        // Kiểm tra xem comment cha có được tìm thấy không trước khi chuyển hướng
        var parentComment = await _context.Comments.FindAsync(parentId);

        return RedirectToAction("DetailPost", "Forum", new { id = parentComment.PostId });


    }




}
