﻿using DoAnWeb.Data;
using DoAnWeb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DoAnWeb.Controllers
{
    [Authorize(Roles =SD.Role_Admin)]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult CustomerList()
        {
            // Lấy danh sách các khách hàng từ cơ sở dữ liệu
            List<Customer> customers = _context.customers.ToList();

            // Trả về view "CustomerList" và truyền danh sách khách hàng vào view
            return View("CustomerList", customers);
        }

        public async Task<IActionResult> Accounts()
        {
            // Lấy danh sách tài khoản từ bảng AspNetUsers và kết hợp với thông tin nhân viên từ bảng Employees
            var usersWithEmployeeInfo = await _context.Users
                .Include(u => u.employees) // Kết hợp với thông tin nhân viên
                .ToListAsync();

            // Trả về view "Accounts" và truyền danh sách tài khoản kết hợp thông tin nhân viên vào view
            return View("Accounts", usersWithEmployeeInfo);
        }


    }
}
