﻿using DoAnWeb.Data;
using DoAnWeb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DoAnWeb.Controllers
{
    [Authorize(Roles = SD.Role_Admin)]
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Categories
        public IActionResult Index()
        {
            var categories = _context.categories.ToList();
            return View(categories);
        }

        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(Category category)
        {
            if (category.name != null)
            {
                _context.categories.Add(category);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var category = await _context.categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            try
            {
                _context.categories.Remove(category);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (DbUpdateException ex)
            {
                // Log the error
                ModelState.AddModelError("", $"Error deleting category: {ex.Message}");
                return View(category);
            }
        }

        public async Task<IActionResult> Update(int id)
        {
            var category = await _context.categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, Category category)
        {

            if (id != category.categoryId)
            {
                return NotFound();
            }
            if (category.name != null )
            {
                try
                {
                    var existingCategory = await _context.categories.FindAsync(id);
                    existingCategory.name = category.name;
                    existingCategory.description = category.description;
                    _context.Update(existingCategory);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Xử lý khi xảy ra lỗi cập nhật
                    return NotFound();
                }
            }
            return View(category);
        }

        private bool CategoryExists(int id)
        {
            return _context.categories.Any(e => e.categoryId == id);
        }
        public IActionResult Search(string searchString)
        {
            var categories = _context.categories.Where(c => c.name.Contains(searchString)).ToList();
            return View("Index", categories);
        }
    }
}
