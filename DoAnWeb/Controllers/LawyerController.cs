﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DoAnWeb.Data;
using DoAnWeb.Models;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.IO.Image;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace DoAnWeb.Controllers
{
    [Authorize(Roles = SD.Role_Admin)]
    public class LawyerController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public LawyerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // Action để hiển thị danh sách luật sư
        public IActionResult Index()
        {
            List<Employee> lawyers = _context.employees.ToList();
            return View(lawyers);
        }

        // Action để hiển thị thông tin cá nhân của luật sư
        public async Task<IActionResult> UserProfile(Employee employee)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            employee.employeeId = currentUser.employeeId;
            var employeeInfo = await _context.employees.FindAsync(employee.employeeId);
            if (employeeInfo == null)
            {
                return NotFound();
            }
            return View("UserProfile", employeeInfo);
        }

        // Action để thêm một luật sư mới
        public async Task<IActionResult> Add()
        {
            //Hiển listPosition
            var positions = await _context.positions.ToListAsync();
            var positionItems = positions.Select(p => new SelectListItem
            {
                Value = p.positionId.ToString(),
                Text = p.positionName
            }).ToList();

            ViewBag.Positions = positionItems;

            //Hiển listCategory
            var categories = await _context.categories.ToListAsync();
            var categoryItems = categories.Select(c => new SelectListItem
            {
                Value = c.categoryId.ToString(),
                Text = c.name
            }).ToList();

            ViewBag.Categories = categoryItems;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(Employee employee, IFormFile image)
        {
            if (employee.Address != null && employee.PhoneNumber != null && employee.FullName != null && employee.Expertise != null && employee.Email != null)
            {
                // Lấy thông tin vị trí từ cơ sở dữ liệu dựa trên positionId được gửi từ form
                employee.positions = await _context.positions.FindAsync(employee.positionId);

                // Lấy thông tin danh mục từ cơ sở dữ liệu dựa trên CategoryId được gửi từ form
                employee.categories = await _context.categories.FindAsync(employee.categoryId);

                // Kiểm tra xem người dùng đã chọn hình ảnh chưa
                if (image != null && image.Length > 0)
                {
                    // Lưu hình ảnh và nhận đường dẫn
                    employee.ImageUrl = await SaveImage(image);
                }

                _context.employees.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                // Nếu có lỗi, trả về lại view "Add" với model chứa thông tin đã được nhập vào
                var positions = await _context.positions.ToListAsync();
                var positionItems = positions.Select(p => new SelectListItem
                {
                    Value = p.positionId.ToString(),
                    Text = p.positionName
                }).ToList();
                ViewBag.Positions = positionItems;

                var categories = await _context.categories.ToListAsync();
                var categoryItems = categories.Select(c => new SelectListItem
                {
                    Value = c.categoryId.ToString(),
                    Text = c.name
                }).ToList();
                ViewBag.Categories = categoryItems;
                // Thêm các thông báo lỗi vào ModelState để hiển thị cho người dùng

                return View(employee);
            }
        }

        public async Task<IActionResult> Update(int id)
        {
            var employee = await _context.employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            //Hiển listPosition
            var positions = await _context.positions.ToListAsync();
            var positionItems = positions.Select(p => new SelectListItem
            {
                Value = p.positionId.ToString(),
                Text = p.positionName
            }).ToList();

            ViewBag.Positions = positionItems;

            //Hiển listCategory
            var categories = await _context.categories.ToListAsync();
            var categoryItems = categories.Select(c => new SelectListItem
            {
                Value = c.categoryId.ToString(),
                Text = c.name
            }).ToList();

            ViewBag.Categories = categoryItems;

            return View(employee);
        }

        [HttpPost]
        public async Task<IActionResult> Update(int id, Employee employee, IFormFile image)
        {
            // Kiểm tra xem id của nhân viên có tồn tại trong cơ sở dữ liệu không
            if (id != employee.employeeId)
            {
                return NotFound();
            }

            // Kiểm tra tính hợp lệ của dữ liệu đầu vào
            if (employee.Address != null)
            {
                try
                {
                    // Lấy nhân viên từ cơ sở dữ liệu
                    var existingEmployee = await _context.employees.FindAsync(id);

                    if (existingEmployee == null)
                    {
                        return NotFound();
                    }

                    // Cập nhật thông tin của nhân viên
                    existingEmployee.FullName = employee.FullName;
                    existingEmployee.DateOfBirth = employee.DateOfBirth;
                    existingEmployee.PhoneNumber = employee.PhoneNumber;
                    existingEmployee.Email = employee.Email;
                    existingEmployee.Address = employee.Address;
                    existingEmployee.Expertise = employee.Expertise;
                    existingEmployee.positionId = employee.positionId;
                    existingEmployee.categoryId = employee.categoryId;

                    // Kiểm tra và cập nhật hình ảnh nếu có
                    if (image != null && image.Length > 0)
                    {
                        // Lưu hình ảnh mới và cập nhật đường dẫn
                        existingEmployee.ImageUrl = await SaveImage(image);
                    }

                    // Cập nhật thông tin của nhân viên trong cơ sở dữ liệu
                    _context.Update(existingEmployee);
                    await _context.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Xử lý khi xảy ra lỗi cập nhật
                    return NotFound();
                }
            }

            // Nếu dữ liệu không hợp lệ, hiển thị lại form cập nhật với thông tin đã nhập và thông báo lỗi
            var positions = await _context.positions.ToListAsync();
            var positionItems = positions.Select(p => new SelectListItem
            {
                Value = p.positionId.ToString(),
                Text = p.positionName
            }).ToList();
            ViewBag.Positions = positionItems;

            var categories = await _context.categories.ToListAsync();
            var categoryItems = categories.Select(c => new SelectListItem
            {
                Value = c.categoryId.ToString(),
                Text = c.name
            }).ToList();
            ViewBag.Categories = categoryItems;

            return View(employee);
        }


        // Action để xóa một luật sư
        public async Task<IActionResult> Delete(int id)
        {
            var employee = await _context.employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            try
            {
                _context.employees.Remove(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Error deleting employee: {ex.Message}");
                return View(employee);
            }
        }

        public async Task<IActionResult> SearchEmployees(string searchTerm)
        {
            var employees = await _context.employees
                .Where(e => e.FullName.Contains(searchTerm) ||
                            e.PhoneNumber.Contains(searchTerm) ||
                            e.Email.Contains(searchTerm) ||
                            e.Address.Contains(searchTerm) ||
                            e.Expertise.Contains(searchTerm) ||
                            e.academicQualification.Contains(searchTerm))
                .ToListAsync();

            return View(employees);
        }

        // Action để tải xuống tất cả thông tin của một luật sư dưới dạng PDF
        public async Task<IActionResult> DownloadAllInfo(int id)
        {
            var lawyer = await _context.employees
                .Include(l => l.positions)
                .Include(l => l.categories)
                .Include(l => l.ratingLawyers)
                .Include(l => l.feedBacks)
                .FirstOrDefaultAsync(l => l.employeeId == id);

            if (lawyer == null)
            {
                return NotFound();
            }

            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    var writer = new PdfWriter(memoryStream);
                    var pdf = new PdfDocument(writer);
                    var document = new Document(pdf);

                    // Tạo bảng
                    Table table = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();

                    // Fetch image data from URL
                    //byte[] imageData;
                    //using (var client = new HttpClient())
                    //{
                    //    imageData = await client.GetByteArrayAsync(lawyer.ImageUrl);
                    //}

                    //// Thêm hình ảnh của luật sư vào ô đầu tiên của hàng đầu tiên
                    //if (imageData != null && imageData.Length > 0)
                    //{
                    //    using (var imageStream = new MemoryStream(imageData))
                    //    {
                    //        var image = new Image(ImageDataFactory.Create(imageStream));
                    //        table.AddCell(new Cell().Add(image).SetHorizontalAlignment(HorizontalAlignment.CENTER));
                    //    }
                    //}
                    //else
                    //{
                    //    table.AddCell(new Cell().Add(new Paragraph("No image available")).SetHorizontalAlignment(HorizontalAlignment.CENTER));
                    //}

                    // Thêm các thông tin cơ bản vào ô thứ hai của hàng đầu tiên
                    var infoCell = new Cell().Add(new Paragraph($"Full Name: {lawyer.FullName}\n")
                        .Add($"Date of Birth: {lawyer.DateOfBirth}\n")
                        .Add($"Phone Number: {lawyer.PhoneNumber}\n")
                        .Add($"Email: {lawyer.Email}\n")
                        .Add($"Address: {lawyer.Address}\n")
                        .Add($"Expertise: {lawyer.Expertise}\n")
                        .Add($"Position: {lawyer.positions?.positionName}\n")
                        .Add($"Position Description: {lawyer.positions?.positionDescription}\n")
                        .Add($"Position Salary: {lawyer.positions?.positionSalary}\n")
                        .Add($"Category: {lawyer.categories?.name}\n"));
                    table.AddCell(infoCell);

                    // Thêm thông tin về ratingLawyers vào bảng
                    var ratingsCell = new Cell().Add(new Paragraph("Rating Lawyers:"));
                    foreach (var rating in lawyer.ratingLawyers)
                    {
                        ratingsCell.Add(new Paragraph($"Rating: {rating.rating}, Comment: {rating.comment}, Rated At: {rating.ratedAt}\n"));
                    }
                    table.AddCell(ratingsCell);

                    // Thêm thông tin về feedBacks vào bảng
                    var feedbacksCell = new Cell().Add(new Paragraph("Feedbacks:"));
                    foreach (var feedback in lawyer.feedBacks)
                    {
                        feedbacksCell.Add(new Paragraph($"Content: {feedback.content}, Rating: {feedback.rating}, Sender Name: {feedback.senderName}\n"));
                    }
                    table.AddCell(feedbacksCell);

                    // Thêm bảng vào tài liệu PDF
                    document.Add(table);

                    // Đóng tài liệu PDF
                    document.Close();

                    // Trả về nội dung của tệp PDF dưới dạng một dãy byte
                    return File(memoryStream.ToArray(), "application/pdf", $"{lawyer.FullName}_Info.pdf");

                }
            }
            catch (Exception ex)
            {
                // Xử lý lỗi và hiển thị thông báo cho người dùng
                return BadRequest($"Error generating PDF: {ex.Message}");
            }
        }




        // Phương thức để lấy danh sách các mục SelectList từ DbSet
        private async Task<List<SelectListItem>> GetSelectListItemsAsync<TEntity>(DbSet<TEntity> entities) where TEntity : class
        {
            var items = await entities.ToListAsync();
            return items.Select(x => new SelectListItem
            {
                Value = x.GetType().GetProperty($"{x.GetType().Name}Id")?.GetValue(x).ToString(),
                Text = x.ToString()
            }).ToList();
        }

        // Phương thức để lưu hình ảnh vào thư mục và trả về đường dẫn
        private async Task<string> SaveImage(IFormFile image)
        {
            var imageName = Guid.NewGuid().ToString() + Path.GetExtension(image.FileName);
            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images", imageName);

            using (var stream = new FileStream(imagePath, FileMode.Create))
            {
                await image.CopyToAsync(stream);
            }

            return "/Images/" + imageName;
        }
    }
}
