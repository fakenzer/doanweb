﻿using DoAnWeb.Data;
using DoAnWeb.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace DoAnWeb.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            List<New> news = _context.news.ToList();
            return View(news);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult AboutUs()
        {
            return View();
        }
        public IActionResult Lawyer()
        {
            List<Employee> lawyers = _context.employees.ToList();
            return View(lawyers);
        }
        //public async Task<IActionResult> EmployeeList(int page = 1, int pageSize = 10)
        //{
        //    var employees = await _context.Employees
        //        .Skip((page - 1) * pageSize)
        //        .Take(pageSize)
        //        .ToListAsync();

        //    return View("EmployeeList", employees);
        //}
        [HttpPost]
        public IActionResult SaveCustomer(Customer customer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.customers.Add(customer);
                    _context.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "An error occurred while saving customer data.");
                    _logger.LogError(ex, "Error saving customer data");
                    return View("Index");
                }
            }
            return View("Index", customer);
        }
        public async Task<IActionResult> UserProfile(Employee employee)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            employee.employeeId = currentUser.employeeId;
            var employeeInfo = await _context.employees.FindAsync(employee.employeeId);
            if (employeeInfo == null)
            {
                return NotFound();
            }
            return View("UserProfile", employeeInfo);
        }
        [HttpPost]
        public async Task<IActionResult> UserProfile(Employee employee, IFormFile image)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            employee.employeeId = currentUser.employeeId;
            if (employee.employeeId != null)
            {
                try
                {
                    // Lấy nhân viên từ cơ sở dữ liệu
                    var existingEmployee = await _context.employees.FindAsync(employee.employeeId);

                    if (existingEmployee == null)
                    {
                        return NotFound();
                    }

                    // Cập nhật thông tin của nhân viên
                    existingEmployee.FullName = employee.FullName;
                    existingEmployee.DateOfBirth = employee.DateOfBirth;
                    existingEmployee.PhoneNumber = employee.PhoneNumber;
                    existingEmployee.Email = employee.Email;
                    existingEmployee.Address = employee.Address;
                    existingEmployee.Expertise = employee.Expertise;
                    existingEmployee.positionId = 1; // Đã sửa lại thành PositionId
                    existingEmployee.categoryId = 1; // Đã sửa lại thành CategoryId

                    // Kiểm tra và cập nhật hình ảnh nếu có
                    if (image != null && image.Length > 0)
                    {
                        // Lưu hình ảnh mới và cập nhật đường dẫn
                        existingEmployee.ImageUrl = await SaveImage(image);
                    }

                    // Cập nhật thông tin của nhân viên trong cơ sở dữ liệu
                    _context.employees.Update(existingEmployee);
                    await _context.SaveChangesAsync();

                    return RedirectToAction(nameof(UserProfile));
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Xử lý khi xảy ra lỗi cập nhật
                    return NotFound();
                }
            }
            return View(employee);
        }


        private async Task<string> SaveImage(IFormFile image)
        {
            var imageName = Guid.NewGuid().ToString() + Path.GetExtension(image.FileName);
            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images", imageName);

            using (var stream = new FileStream(imagePath, FileMode.Create))
            {
                await image.CopyToAsync(stream);
            }

            return "/Images/" + imageName;
        }
    }

}
