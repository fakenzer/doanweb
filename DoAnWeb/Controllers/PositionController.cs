﻿using DoAnWeb.Data;
using DoAnWeb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DoAnWeb.Controllers
{
    [Authorize(Roles = SD.Role_Admin)]
    public class PositionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PositionController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            List<Position> positions = _context.positions.ToList();
            return View(positions);
        }
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(Position position)
        {
            if (position.positionSalary != null && position.positionName != null)
            {
                _context.positions.Add(position);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(position);
        }
        public async Task<IActionResult> Delete(int id)
        {
            var position = await _context.positions.FindAsync(id);
            try
            {
                _context.positions.Remove(position);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Xử lý lỗi và hiển thị thông báo cho người dùng
                ModelState.AddModelError("", $"Error deleting position: {ex.Message}");
                return View(position);
            }
        }
        public async Task<IActionResult> Update(int id)
        {


            var position = await _context.positions.FindAsync(id);
            if (position == null)
            {
                return NotFound();
            }

            return View(position);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id,  Position position)
        {
            if (id != position.positionId)
            {
                return NotFound();
            }
            if (position.positionName != null && position.positionSalary != null )
            {
                try
                {
                    var existingPosition = await _context.positions.FindAsync(id);
                    existingPosition.positionName = position.positionName;
                    existingPosition.positionDescription = position.positionDescription;
                    existingPosition.positionSalary = position.positionSalary;
                    _context.Update(existingPosition);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Xử lý khi xảy ra lỗi cập nhật
                    return NotFound();
                }
            }
            return View(position);
        }

        private bool PositionExists(int id)
        {
            return _context.positions.Any(e => e.positionId == id);
        }
    }
}
