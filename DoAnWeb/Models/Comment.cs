﻿using Microsoft.Extensions.Hosting;

namespace DoAnWeb.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public string Author { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public List<Reply>? Replies { get; set; }
    }

}
