﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Article
    {
        [Key]
        public int articleId { get; set; }
        public required string title { get; set; }
        public required string Content { get; set; }
        public string? Author { get; set; }
        [Required]
        private DateTime _publishDate;

        public DateTime publishDate
        {
            get
            {
                return _publishDate;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    _publishDate = value;
                }
                else
                {
                    throw new ArgumentException("Ngày đăng không thể lớn hơn ngày hiện tại.");
                }
            }
        }
        public string? poster { get; set; }
        public int categoryId { get; set; }
        public required Category categories { get; set; }
    }
}
