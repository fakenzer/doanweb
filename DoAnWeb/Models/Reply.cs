﻿namespace DoAnWeb.Models
{
    public class Reply
    {
        public int ReplyId { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public string Author { get; set; }
        public int CommentId { get; set; }
        public Comment Comments { get; set; }
    }

}
