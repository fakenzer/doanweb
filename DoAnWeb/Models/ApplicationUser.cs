﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int employeeId { get; set; }
        public required Employee employees { get; set; }
    }
}
