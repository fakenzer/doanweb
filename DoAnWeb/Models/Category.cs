﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Category
    {
        [Key]
        public int categoryId { get; set; }
        public required string name { get; set; }
        public string? description { get; set; }
        public required ICollection<New> news { get; set; }
        public required ICollection<LegalDocument> legalDocuments { get; set; }
        public required ICollection<FAQ> faqs { get; set; }
        public required ICollection<Employee> employees { get; set; }
        public required ICollection<Article> articles { get; set; }
        public required ICollection<Service> services { get; set; }
    }
}
