﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class New
    {
        [Key]
        public int newId { get; set; }
        public required string title { get; set; }
        public required string content { get; set; }
        public string? author { get; set; }
        [Required]
        private DateTime _publishDate;

        public DateTime publishDate
        {
            get
            {
                return _publishDate;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    _publishDate = value;
                }
                else
                {
                    throw new ArgumentException("Ngày đăng không thể lớn hơn ngày hiện tại.");
                }
            }
        }
        public int categoryId { get; set; }
        public required Category categories { get; set; }
    }
}
