﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class FeedBack
    {
        [Key]
        public int feebBackId { get; set; }
        public required string content { get; set; }
        [Required]
        public int rating { get; set; }
        public required string senderName { get; set; }
        public int employeeId { get; set; }
        public required Employee employees { get; set; }
        public int customerId { get; set; }
        public required Customer customers { get; set; }
    }
}
