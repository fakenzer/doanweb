﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Customer
    {
        [Key]
        public int customerId { get; set; }
        public required string customerName { get; set; }
        public required string email { get; set; }
        [MaxLength(10)]
        public required string phoneNumber { get; set; }
        public string? address { get; set; }
        public string? Notes { get; set; }
    }
}
