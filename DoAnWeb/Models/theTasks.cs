﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class theTasks
    {
        [Key]
        public int taskId { get; set; }
        public string? description { get; set; }
        public required string status { get; set; }
        [Required]
        private DateTime _createdAt;

        public DateTime createdAt
        {
            get
            {
                return _createdAt;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    _createdAt = value;
                }
                else
                {
                    throw new ArgumentException("Ngày tạo không thể lớn hơn ngày hiện tại.");
                }
            }
        }
        private DateTime _dateEnd;
        public DateTime dateEnd
        {
            get
            {
                return _dateEnd;
            }
            set
            {
                if (value > createdAt)
                {
                    _dateEnd = value;
                }
                else
                {
                    throw new ArgumentException("Ngày kết thúc phải sau ngày tạo.");
                }
            }
        }
        public int employeeId { get; set; }
        public required Employee employees { get; set; }
        public int customerId { get; set; }
        public required Customer customers { get; set; }
    }
}
