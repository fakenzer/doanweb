﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class LegalDocument
    {
        [Key]
        public int legalDocumentId { get; set; }
        public required string Title { get; set; }
        public required string Description { get; set; }
        public required string imgUrl { get; set; }
        public required string author { get; set; }
        [Required]
        private DateTime _publishDate;

        public DateTime publishDate
        {
            get
            {
                return _publishDate;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    _publishDate = value;
                }
                else
                {
                    throw new ArgumentException("Ngày đăng không thể lớn hơn ngày hiện tại.");
                }
            }
        }
        public int categoryId { get; set; }
        public required Category categories { get; set; }
    }
}
