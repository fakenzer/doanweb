﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Service
    {
        [Key]
        public int serviceId { get; set; }
        public required string name { get; set; }
        public string? Description { get; set; }
        [Required]
        public double priced { get; set; }
        public required string relatedExpertise { get; set; }
        public int categoryId { get; set; }
        public required Category categories { get; set; }
    }
}
