﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class RatingLawyer
    {
        [Key]
        public int ratingId { get; set; }
        [Required]
        public int rating { get; set; }
        public string? comment { get; set; }
        [Required]
        private DateTime _ratedAt;

        public DateTime ratedAt
        {
            get
            {
                return _ratedAt;
            }
            set
            {
                if (value <= DateTime.Now)
                {
                    _ratedAt = value;
                }
                else
                {
                    throw new ArgumentException("Ngày đánh giá không thể lớn hơn ngày hiện tại.");
                }
            }
        }
        public int employeeId { get; set; }
        public required Employee employees { get; set; }
        public int customerId { get; set; }
        public required Customer customers { get; set; }
    }
}
