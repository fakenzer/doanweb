﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class FAQ
    {
        [Key]
        public int faqId { get; set; }
        public required string question { get; set; }
        public string? answer { get; set; }
        public int categoryId { get; set; }
        public required Category categories { get; set; }
    }
}
