﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Position
    {
        [Key]
        public int positionId { get; set; }
        [MaxLength(50)]
        public required string positionName { get; set; }
        public string? positionDescription { get; set; }
        [Required]
        public decimal positionSalary { get; set; }
        public required ICollection<Employee> employees { get; set; }
    }
}
