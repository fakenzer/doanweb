﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Employee
    {
        [Key]
        public int employeeId { get; set; }
        [MaxLength(50)]
        public required string FullName { get; set; }
        private DateTime _dateOfBirth;
        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                if (DateTime.Today.Year - value.Year < 20)
                {
                    throw new ArgumentException("Ngày sinh phải trước ít nhất 20 năm so với ngày hiện tại.");
                }
                _dateOfBirth = value;
            }
        }
        [MaxLength(10)]
        public required string PhoneNumber { get; set; }
        public required string Email { get; set; }
        public string? Address { get; set; }
        public required string ImageUrl { get; set; }
        [MaxLength(50)]
        public required string Expertise { get; set; }
        public DateTime? commencementDate { get; set; }
        public required string academicQualification { get; set; }
        public string? facebookURL { get; set; }
        public string? instagramURL { get; set; }
        public string? twitterURL { get; set; }
        public int positionId { get; set; }
        public required Position positions { get; set; }

        public int categoryId { get; set; }
        public required Category categories { get; set; }
        public required ICollection<RatingLawyer> ratingLawyers { get; set; }
        public required ICollection<FeedBack> feedBacks { get; set; }
    }
}
