﻿namespace DoAnWeb.Models
{
    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public required string ImageUrl { get; set; }
        public DateTime DateCreated { get; set; }

        public int employeeId { get; set; }
        public Employee employees { get; set; }

        public List<Comment>? Comments { get; set; }
    }

}
