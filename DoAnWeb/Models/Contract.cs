﻿using System.ComponentModel.DataAnnotations;

namespace DoAnWeb.Models
{
    public class Contract
    {
        [Key]
        public int contractId { get; set; }
        public required string status { get; set; }
        public string? Description { get; set; }
        [Required]
        public double amount { get; set; }
        [Required]
        public DateTime startDate { get; set; }

        [Required]
        public DateTime endDate
        {
            get => endDate;
            set
            {
                if (value < startDate)
                {
                    throw new ArgumentException("Ngày kết thúc phải sau ngày bắt đầu.");
                }
                endDate = value;
            }
        }
        [Required]
        public DateTime createdAt { get; set; } = DateTime.Today;
        public int employeeId { get; set; }
        public required Employee employees { get; set; }
        public int customerId { get; set; }
        public required Customer customers { get; set; }
    }
}
