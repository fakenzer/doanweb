﻿const list = document.querySelector('.image-list');
const items = document.querySelectorAll('.image-list .item');
let active = 0;
const lastItemIndex = items.length - 1;
if (items[0]) {
    let width = items[0].offsetWidth;
    function transitionSlide() {
        list.style.transition = 'transform 0.5s ease';
        list.style.transform = `translateX(${width * -1 * active}px)`;
    }

    function nextSlide() {
        if (active === lastItemIndex) {
            active = 0;
        } else {
            active++;
        }
        transitionSlide();
    }

    setInterval(nextSlide, 4000);
}


