﻿$(document).ready(function () {
    function checkWidthAndRedirect() {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (width < 1400) {
            alert("Bạn đã làm sai rồi,Admin không thể nào sử dụng ở trình duyệt có chiều rộng nhỏ hơn 1400px");
            window.location.href = "/Home/Index";
        }
    }

    checkWidthAndRedirect();

    $(window).resize(function () {
        checkWidthAndRedirect();
    });
});