# DoAnWeb



## Getting started
To install this project, follow these steps:

1) Begin by cloning the project onto your computer.
2) Edit your database configuration in the appsettings.json file.
3) Next, navigate to the toolbar and select "Tools," then "NuGet Package Manager," and finally "Package Manager Console."
4) In the Package Manager Console, execute the command Update-Database to update your database.
5) Then, navigate to the AdminController within the Controllers folder and insert the following code: [Authorize(Roles =SD.Role_Admin)].
6) Similarly, apply the same code to the Register file located at Areas/Identity/Pages/Account/Register.cshtml.cs.
7) Afterward, run the code. In the host, append /admin to the URL and proceed to add categories, positions, and employees in the specified order.
8) Visit the account section and click on the "Register" button to assign the admin role to your account.
9) Finally, remove the [Authorize(Roles =SD.Role_Admin)] command from the code.


## Collaborate with your team

## Authors and acknowledgment
    My team:
    1) Huỳnh Công Tường.
    2) Lưu Quang Khánh.
    3) Lê Duy Luân.
    4) Nguyễn Đức Khánh.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
